resource "aws_lambda_function" "s3_lambda" {
  function_name = "${var.lambda_name}"
  handler = "${var.handler}"
  runtime = "${var.runtime}"
  memory_size = "${var.memory_size}"
  timeout = "${var.timeout}"

  tags = "${var.tags}"

  s3_bucket = "${var.source_bucket}"
  s3_key = "${var.source_key}"

  role = "${aws_iam_role.lambda_role.arn}"

  environment {
    variables = "${merge(var.environment, map("provider", "terraform"))}"
  }
}

resource "aws_iam_role" "lambda_role" {
  name = "${var.lambda_name}-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "cloudwatch_logs" {
  name = "cloudwatch-logging"
  role = "${aws_iam_role.lambda_role.id}"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:*"
            ],
            "Resource": [
                "arn:aws:logs:*:*:*"
            ]
        }
    ]
}
EOF
}
