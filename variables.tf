variable "source_bucket" {
  type = "string"
  description = "Bucket containing the Lambda function"
}

variable "source_key" {
  type = "string"
  description = "Lambda function zip within the source bucket"
}

variable "lambda_name" {
  type = "string"
  description = "Lambda Fucntion Name"
}

variable "handler" {
  type = "string"
  description = "Entrypoint Hanlder Function"
}

variable "runtime" {
  type = "string"
  description = "Lambda Runtime"
}

variable "environment" {
  type = "map"
  description = "Map of Environment Variables to pass to the lambda function"
  default = {
    module = "aws-tf-lambda-s3"
  }
}

variable "memory_size" {
  type = "string"
  description = "Memory allocation for the lambda"
  default = "128"
}

variable "timeout" {
  type = "string"
  description = "Timeout for the Lambda"
  default = "3"
}

variable "tags" {
  type = "map"
  description = "Tags to apply to the Lambda function"
  default = {}
}
