output "function_name" {
  value = "${aws_lambda_function.s3_lambda.function_name}"
}

output "arn" {
  value = "${aws_lambda_function.s3_lambda.arn}"
}

output "invoke_arn" {
  value = "${aws_lambda_function.s3_lambda.invoke_arn}"
}

output "role_id" {
  value = "${aws_iam_role.lambda_role.id}"
}
